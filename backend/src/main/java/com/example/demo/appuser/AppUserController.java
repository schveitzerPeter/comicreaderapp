package com.example.demo.appuser;


import com.example.demo.responses.LoggedInResponse;
import com.example.demo.responses.ResponseSuccess;
import com.example.demo.responses.UserDataResponse;
import com.example.demo.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@CrossOrigin(origins = "http://localhost:4200", methods = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET, RequestMethod.DELETE})
@RequestMapping(path = "api/v1/auth")
public class AppUserController {

    @Autowired
    private AuthenticationManager authenticationManager;
    public void setAuthenticationManager(AuthenticationManager authenticationManager) { this.authenticationManager = authenticationManager; }

    @Autowired
    private AppUserService appUserService;
    public void setAppUserService(AppUserService appUserService) { this.appUserService = appUserService; }

    @Autowired
    private JwtUtil jwtUtil;




    @GetMapping(value = "/isLoggedIn")
    public ResponseEntity<?> isLoggedIn(
            @RequestHeader (value = "Authorization", required = false) String token
    ){
        if(token != null){
            final String userName = jwtUtil.extractUsername(token);
            final UserDetails userDetails = appUserService.loadUserByUsername(userName);
            final boolean isTokenValid = jwtUtil.validateToken(token,userDetails);
            final AppUser currentUser = (AppUser) appUserService.loadUserByUsername(userName);
            final UserDataResponse userDataResponse = new UserDataResponse(
                    currentUser.getId(),
                    currentUser.getFirstname(),
                    currentUser.getLastname(),
                    currentUser.getComics()
            );
            if(isTokenValid){
                return ResponseEntity.ok( userDataResponse.toString() );
            }
        }
        return ResponseEntity.ok(false);
    }


    @PostMapping(value= "/logout")
    public ResponseEntity<?> logout(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        System.out.println(currentPrincipalName);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

        AppUser currUser = (AppUser) appUserService.loadUserByUsername(currentPrincipalName);
        System.out.println(currUser);
        return ResponseEntity.ok(true);
    }


    @PostMapping(value = "/login")
    public ResponseEntity<?> authenticate(@RequestBody AuthentiactionRequest authentiactionRequest) throws  Exception{
        System.out.println(authentiactionRequest);
        try {
            UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(authentiactionRequest.getUsername(), authentiactionRequest.getPassword());
            Authentication auth = authenticationManager.authenticate(authReq);
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(auth);


            /*authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authentiactionRequest.getUsername(),authentiactionRequest.getPassword())
            );*/
        }catch (BadCredentialsException e){
            throw new Exception("Incorrect username or password", e);
        }
        final UserDetails userDetails = appUserService.loadUserByUsername(authentiactionRequest.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }
}
