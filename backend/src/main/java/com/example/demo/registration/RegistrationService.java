package com.example.demo.registration;

import com.example.demo.appuser.AppUser;
import com.example.demo.appuser.AppUserRole;
import com.example.demo.appuser.AppUserService;
import com.example.demo.email.EmailSender;
import com.example.demo.registration.token.ConfirmationToken;
import com.example.demo.registration.token.ConfirmationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;

@Service

public class RegistrationService {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private ConfirmationTokenService confirmationTokenService;

    @Autowired
    private EmailSender emailSender;

    public void setEmailSender(EmailSender emailSender) { this.emailSender = emailSender; }

    public void setAppUserService(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    public void setConfirmationTokenService(ConfirmationTokenService confirmationTokenService) { this.confirmationTokenService = confirmationTokenService; }



    public String register(RegistrationRequest request)  {

         String token = appUserService.signUpUser(
                 new AppUser(
                         request.getFirstName(),
                         request.getLastName(),
                         request.getEmail(),
                         request.getPassword(),
                         AppUserRole.USER,
                         false,
                         false
                 )
         );
        String  link = "http://localhost:8080/api/v1/registration/confirm?token="+token;
        link = "<a href='"+link+"'>"+link+"</a>";

        String emailText = "Thank you for registration " + request.getFirstName() + "" +
                "All you have to now it just confirm your email address with the link below:\n" +link;
        emailSender.send(request.getEmail(), emailText );
        return token;
    }

    public String confirmToken(String token) {
        ConfirmationToken confirmationToken = confirmationTokenService
                .getToken(token)
                .orElseThrow(() ->
                        new IllegalStateException("token not found"));

        if (confirmationToken.getConfirmedAt() != null) {
            throw new IllegalStateException("email already confirmed");
        }

        LocalDateTime expiredAt = confirmationToken.getExpiredAt();

        if (expiredAt.isBefore(LocalDateTime.now())) {
            throw new IllegalStateException("token expired");
        }

        confirmationTokenService.setConfirmedAt(token);
        appUserService.enableAppUser(
                confirmationToken.getAppUser().getUsername());
        return "confirmed";
    }
}
