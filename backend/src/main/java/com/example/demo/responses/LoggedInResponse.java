package com.example.demo.responses;

import java.util.Date;

public class LoggedInResponse {
    private String userName;
    private Date expirationDate;

    public LoggedInResponse(String userName, Date expirationDate) {
        this.userName = userName;
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "{ \"username\" : \""+userName+"\", \"expirationdate\" : \""+expirationDate+"\" }";
    }
}
