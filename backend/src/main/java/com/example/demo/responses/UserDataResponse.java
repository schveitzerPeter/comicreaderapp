package com.example.demo.responses;

import com.example.demo.comic.Comic;

import java.util.Set;

public class UserDataResponse {
    private long userId;
    private String firstName;
    private String lastName;
    private Set<Comic> comicSet;

    public UserDataResponse(long userId, String firstName, String lastName, Set<Comic> comicSet) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.comicSet = comicSet;
    }

    @Override
    public String toString() {
        return "{ \"userId\": \""+userId+"\"," +
                " \"firstName\": \""+firstName+"\"," +
                " \"comics\": "+comicSet+"," +
                " \"lastName\": \""+lastName+"\"}";
    }
}
