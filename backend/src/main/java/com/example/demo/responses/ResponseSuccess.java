package com.example.demo.responses;

public class ResponseSuccess {
    private String status;
    private Object data;

    public ResponseSuccess(String status, Object data) {
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{  \"status \": \""+status+"\", \"data\": "+data+" }";
    }
}
