package com.example.demo.responses;

public class ResponseError {
    private String status;
    private String message;
    private String data;

    public ResponseError(String status, String message, String data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    @Override
    public String toString() {
        return" \"status\" : \" "+ status +", \"data\" : \" "+data+" \", \"message\": \" "+message+" \" ";
    }
}
