package com.example.demo.comic;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
@NoArgsConstructor
public class Page {
    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Comic comic;
    private String pageSrc;

    public Page(String originalFilename, long comicId) {
    }

    public long getId() {
        return id;
    }

    public Comic getComic() {
        return comic;
    }

    public String getPageSrc() {
        return pageSrc;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setComic(Comic comic) {
        this.comic = comic;
    }

    public void setPageSrc(String pageSrc) {
        this.pageSrc = pageSrc;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +","+
                "\"pageSrc\":\"" + pageSrc+"\"}";
    }
}
