package com.example.demo.comic;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ComicRepository extends JpaRepository<Comic, Long> {

    Comic findById( long id);
    void deleteById( long id);
    Comic save(Comic comic);
}
