package com.example.demo.comic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ComicService {

    ComicRepository comicRepository;

    @Autowired
    public void setComicRepository(ComicRepository comicRepository) {
        this.comicRepository = comicRepository;
    }

    public Comic findById( long id){
        return comicRepository.findById(id);
    }

    public void deleteById(long id) {
        comicRepository.deleteById(id);
    }

    public Comic saveComic(Comic comic){
        return comicRepository.save(comic);
    }


}
