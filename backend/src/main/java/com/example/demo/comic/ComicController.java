package com.example.demo.comic;

import com.example.demo.appuser.AppUser;
import com.example.demo.appuser.AppUserRole;
import com.example.demo.appuser.AppUserService;
import com.example.demo.responses.AddComicRequest;
import com.example.demo.responses.ResponseSuccess;
import com.example.demo.responses.UserDataResponse;
import com.example.demo.util.JwtUtil;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import javax.persistence.EntityExistsException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200", methods = {RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET, RequestMethod.DELETE})
@RestController
@RequestMapping(path = "api/v1/comic")
public class ComicController {

    private ComicService comicService;

    @Autowired
    ServletContext context;

    @Autowired
    public void setComicService(ComicService comicService) {
        this.comicService = comicService;
    }

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AppUserService appUserService;
    public void setAppUserService(AppUserService appUserService) { this.appUserService = appUserService; }

    // GET
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public ResponseEntity findById(@PathVariable(value = "id") long id){

        Comic comic = comicService.findById(id);;
        if(comic == null){
            return ResponseEntity.ok("{ \"message\": \"No comic with id: "+id+"\"}");
        }
        return ResponseEntity.ok(comic.toString());
    }

    // DELETE
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE,
            produces = "application/json"
    )
    public ResponseEntity deleteById(@PathVariable(value = "id") long id, @RequestHeader (value = "Authorization", required = false) String token ){
        if(token != null){
            final String userName = jwtUtil.extractUsername(token);
            final UserDetails userDetails = appUserService.loadUserByUsername(userName);

            if(jwtUtil.validateToken(token,userDetails)){
                final AppUser currentUser = (AppUser) appUserService.loadUserByUsername(userName);
                Set<Comic> userComics = currentUser.getComics();
                for( Comic comic: userComics ){
                    if(comic.getId() == id){
                        comicService.deleteById(id);
                        //return ResponseEntity.ok("Comic with id: "+id+" deleted!");
                        return ResponseEntity.ok("{\"message\": \"Comic with id: "+id+" deleted!\"}");
                    }
                }
                return ResponseEntity.ok("This comic is not yours to delete");
            }
            return ResponseEntity.ok("Not valid token");
        }
        return ResponseEntity.ok("No token");
    }

    @RequestMapping(
        value = "addComic",
        method = RequestMethod.POST
    )
    public ResponseEntity saveComic(@RequestBody AddComicRequest comicRequest, @RequestHeader (value = "Authorization", required = false) String token ){

        if(token != null){
            final String userName = jwtUtil.extractUsername(token);
            final UserDetails userDetails = appUserService.loadUserByUsername(userName);

            if(jwtUtil.validateToken(token,userDetails)){
                final AppUser currentUser = (AppUser) appUserService.loadUserByUsername(userName);
                Comic comicToSave = new Comic();
                comicToSave.setAppuser(currentUser);
                comicToSave.setTitle(comicRequest.getTitle());
                try{
                    comicService.saveComic(comicToSave);
                    return ResponseEntity.ok(comicRequest);
                } catch ( Exception  e){
                    return ResponseEntity.ok(e);
                }
            }
        }
        return ResponseEntity.ok(false);
    }

    @RequestMapping(
            value = "pageupload/{comicId}",
            method = RequestMethod.POST
    )
    public ResponseEntity pageUpload( @RequestParam("pages[]") MultipartFile[] images,@PathVariable(value = "comicId") long comicId ) throws IOException {

        String rootPath = System.getProperty("user.dir")+"\\src\\main\\resources\\public\\comics\\"+comicId+"\\";
        File newDirectory = new File(rootPath);
        newDirectory.mkdirs();

        for( MultipartFile img: images) {
            File convertFile = new File( rootPath+img.getOriginalFilename() );
            convertFile.createNewFile();

            try (FileOutputStream fout = new FileOutputStream(convertFile))
            {
                fout.write(img.getBytes());
            }catch (Exception e){
                System.out.println(e);
            }
        }
        return ResponseEntity.ok(" { \"status\":\"ok\"} ");
    }
}
