package com.example.demo.comic;

import com.example.demo.appuser.AppUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Comic {
    @Id
    @GeneratedValue
    private long id;
    private String title;

    @ManyToOne()
    @OnDelete(action = OnDeleteAction.CASCADE)
    private AppUser appuser;

    @OneToMany(mappedBy = "comic")
    private Set<Page> pages;

    public Comic() { }

    public Comic(String title){
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public AppUser getAppuser() {
        return appuser;
    }

    public Set<Page> getPages() {
        return pages;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAppuser(AppUser appuser) {
        this.appuser = appuser;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }


    @Override
    public String toString() {
        return "{ \"comicId\":" +id + ", \"title\":\""+title+"\", \"userId\":"+appuser.getId() +
                ", \"pages\":"+ pages+ " }";
    }
}